package ru.t1.tbobkov.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}
