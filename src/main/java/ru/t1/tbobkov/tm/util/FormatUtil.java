package ru.t1.tbobkov.tm.util;

import java.text.DecimalFormat;

public final class FormatUtil {

    private static final double KILOBYTE = 1024;

    private static final double MEGABYTE = KILOBYTE * 1024;

    private static final double GIGABYTE = MEGABYTE * 1024;

    private static final double TERABYTE = GIGABYTE * 1024;

    private static final String BYTE_NAME = "B";

    private static final String BYTES_NAME_LONG = "Bytes";

    private static final String KILOBYTE_NAME = "KB";

    private static final String MEGABYTE_NAME = "MB";

    private static final String GIGABYTE_NAME = "GB";

    private static final String TERABYTE_NAME = "TB";

    private static final String SEPARATOR = " ";

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.###");

    private static String render(final double bytes) {
        return DECIMAL_FORMAT.format(bytes);
    }

    private static String render(final long bytes, final double size) {
        return render(bytes / size);
    }

    private static String render(final long bytes, final String name) {
        return render(bytes) + SEPARATOR + name;
    }

    private static String render(final long bytes, final double size, final String name) {
        return render(bytes / size) + SEPARATOR + name;
    }

    public static String formatBytes(final long bytes) {
        if ((bytes >= 0) && (bytes < KILOBYTE)) return bytes + SEPARATOR + BYTE_NAME;
        if ((bytes >= KILOBYTE) && (bytes < MEGABYTE)) return render(bytes, KILOBYTE, KILOBYTE_NAME);
        if ((bytes >= MEGABYTE) && (bytes < GIGABYTE)) return render(bytes, MEGABYTE, MEGABYTE_NAME);
        if ((bytes >= GIGABYTE) && (bytes < TERABYTE)) return render(bytes, GIGABYTE, GIGABYTE_NAME);
        if (bytes >= TERABYTE) return render(bytes, TERABYTE, TERABYTE_NAME);
        return render(bytes, BYTES_NAME_LONG);
    }

    private FormatUtil() {
    }

}




